const path = require("path");
const fs = require("fs");

const STATIC_HTML = path.resolve("./html");

const getHtml = (file) => {
  try {
    return fs.readFileSync(
      path.join(STATIC_HTML, "/", `${file}.html`),
      "utf-8"
    );
  } catch (error) {
    return "";
  }
};

const parseHtmlContent = (file, html) => {
  return getHtml(file).replace("__CONTENT__", html || "");
};

const isValidID = (id) => {
  const ID = parseInt(id, 10);
  return Number.isInteger(ID) && ID >= 0 && ID < 4 ;
};

module.exports = { parseHtmlContent, isValidID };
