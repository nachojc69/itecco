const request = require("supertest");
const app = require("../server");

const Content = require("../models/Content");
let cont;

beforeEach(() => {
  cont = new Content();
  app.set("Content", new Content());
});

describe("Test the routes", () => {
  test("default route", (done) => {
    request(app)
      .get("/")
      .then((response) => {
        expect(response.statusCode).toBe(200);
        expect(response.header["content-type"]).toBe(
          "text/html; charset=utf-8"
        );
        expect(response.text.indexOf(cont.buildHtml())).not.toBe(-1);

        done();
      });
  });
  test("add data in form", (done) => {
    request(app)
      .post("/send/2")
      .send({ value: "josh" })
      .then((response) => {
        expect(response.statusCode).toBe(200);
        expect(response.header["content-type"]).toBe(
          "text/html; charset=utf-8"
        );
        expect(response.text.indexOf(cont.buildHtml())).toBe(-1);

        expect(
          response.text.indexOf(cont.setElement("josh", 2).buildHtml())
        ).not.toBe(-1);

        done();
      });
  });

  test("click link", async () => {
    await request(app)
      .post("/send/2")
      .send({ value: "josh" });

    const response = await request(app).get("/start/2");

    expect(response.text.indexOf('<td>josh</td>')).not.toBe(-1);
  });
});
