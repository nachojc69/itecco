const Content = require("../models/Content");
let cont;

beforeEach(() => {
  cont = new Content();
});

test("Model starting", () => {
  expect(typeof cont.current).toBe("object");
  expect(cont.current).toStrictEqual({ main: "First", boxes: [, , ,] });
});

test("Model add element", () => {
  expect(cont.current).toStrictEqual({ main: "First", boxes: [, , ,] });
  cont.setElement("one", 2);
  expect(cont.current.boxes).toStrictEqual([, , "one"]);
  cont.setElement("two", 0);
  expect(cont.current.boxes).toStrictEqual(["two", , "one"]);
});

test("Model set Start", () => {
  expect(cont.current).toStrictEqual({ main: "First", boxes: [, , ,] });
  cont
    .setElement("one", 0)
    .setElement("two", 1)
    .setElement("more", 2)
    .setElement("any", 3);
  expect(cont.current.boxes).toStrictEqual(["one", "two", "more", "any"]);
  cont.setStart(1);
  expect(cont.current).toStrictEqual({ main: 'two', boxes: [,,,] });
});

test("Model reset", () => {
  expect(cont.current).toStrictEqual({ main: "First", boxes: [, , ,] });
  cont.setElement("one", 2);
  expect(cont.current.boxes).toStrictEqual([, , "one"]);
  cont.reset();
  expect(cont.current).toStrictEqual({ main: "First", boxes: [, , ,] });
});
