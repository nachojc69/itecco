class Content {
  current = {};

  constructor() {
    this.reset();
  }

  setElement(value, index) {
    this.current.boxes[index] = value;
    return this;
  }
  setStart(index) {
    this.current.main = this.current.boxes[index];
    this.current.boxes = [, , ,];
    return this;
  }
  reset() {
    this.current = { main: "First", boxes: [, , ,] };
    return this;
  }
  buildHtml() {
    return _buildHtml(this.current.boxes, this.current.main);
  }
}

const _buildHtml = (content, text) => {
  return `
    <tr>${_getHtml(content, 0)}<td></td>${_getHtml(content, 1)}</tr>
    <tr><td></td><td>${text}</td><td></td></tr>
    <tr>${_getHtml(content, 2)}<td></td>${_getHtml(content, 3)}</tr>`;
};

const _getHtml = (text, index) =>
  `<td>${
    text[index]
      ? `<a href="/start/${index}">${text[index]}</a>`
      : `<form method="post" action="/send/${index}"><input name="value"/><input type="submit"/></form>`
  }</td>`;

module.exports = Content;
