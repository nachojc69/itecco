const express = require("express");
const router = express.Router();
const { parseHtmlContent, isValidID } = require("../../utils");

router.get("", async (req, res) => {
  res.setHeader("Content-Type", "text/html");
  const cont = req.app.get("Content");
  cont.reset();

  res.send(parseHtmlContent("main", cont.buildHtml()));
});

router.post("/send/:id", async (req, res) => {
  const cont = req.app.get("Content");
  
  const ID = parseInt(req.params.id, 10);
  if (isValidID(ID)) cont.setElement(req.body.value, req.params.id);

  res.send(parseHtmlContent("main", cont.buildHtml()));
});

router.get("/start/:id", async (req, res) => {
  const cont = req.app.get("Content");

  const ID = parseInt(req.params.id, 10);
  if (isValidID(ID)) cont.setStart(ID);

  res.send(parseHtmlContent("main", cont.buildHtml()));
});

module.exports = router;
