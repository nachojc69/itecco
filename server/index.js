const express = require("express");
var bodyParser = require('body-parser');
const app = express();

const context = require('./middlewares');

app.use(bodyParser.urlencoded({ extended: true })); 
// Middlewares...
app.use(context);

// Routes...
const routes = require("./routes");

app.use(express.json());
app.use(routes);

module.exports = app;
