const Content = require("../../models/Content");

const contextDB = (req, _, next) => {
  // To define more concurrent users here we can have the managemnet storage
  req.app.set("Content", req.app.get("Content") || new Content());

  next();
};

module.exports = contextDB;
